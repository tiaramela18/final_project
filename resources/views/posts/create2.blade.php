@extends('adminlte.master')

@section('content')
            <form role="form" action="/pesan" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="InputJudul">Judul</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Judul" name="judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <!-- input pesan -->
                  <div class="form-group">
                    <label for="InputPesan">Pesan</label>
                    <textarea class="form-control" rows="3" placeholder="Enter ..." name="isi"></textarea>
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <!-- Input gambar -->
                  
                  <div class="form-group">
                    <label for="exampleInputFile">Gambar</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                        <label class="custom-file-label" for="exampleInputFile">Pilih Gambar</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                  <label for="InputPesan">Durasi</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="tanggal" value="1">
                          <label class="form-check-label" >1 tahun</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="tanggal value="2">
                          <label class="form-check-label" >2 tahun</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio"  name="tanggal" value="3">
                          <label class="form-check-label" >3 tahun</label>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="tanggal" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask="" placeholder="yyyy-mm-dd" im-insert="false">
                        </div>
                        <!-- import package kalender -->
                    </div>
                    <div class="form-group">
                    <label for="InputPesan">Access</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="control" value="0">
                          <label class="form-check-label">Private</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="control" value="1">
                          <label class="form-check-label">Public, but anonymous</label>
                        </div> 
                    </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" placeholder="Email" name="email">
                </div>
                  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Send</button>
                </div>
              </form>
          
@endsection