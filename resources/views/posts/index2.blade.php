
@extends('adminlte.master')

@section('content')

<div class="mt-3 ml-3 mr-3">
                
  <h2>Tembok pesan</h2>
  <div class="text-right mb-3"> 
    <a class="btn btn-info" href="/pesan/create"> Buat Pesan</a>
  </div>

  <div class="card-columns">
    @forelse($pesan as $key=> $post)
        
         <div class="card">
          <div class="card-body">
          
            <div class="text-right"><small class="description text-muted">{{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</small></div>
            <h3 class="card-title"><b>{{ $post->judul}}</b></h3>
            <p class="card-text">{{ $post->isi}}</p>

            <img width="150px" src="{{ url('/data_file/'.$post->image) }}">
          </div>
        </div> 
                
        @empty
        <div class="card text-center">
          <div class="card-body">
            <p class="card-text">Belum Ada Pesan. Silahkan Tulis Pesan</p>
          </div>
        </div>
        @endforelse

  </div>
</div>
@endsection