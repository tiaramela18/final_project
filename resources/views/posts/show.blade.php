@extends('adminlte.master')

@section('content')
        <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                      <div class="user-block">
                        <span class="username">
                          <p>Anonim</p>
                        </span>
                        <span class="description">Shared publicly - 7:30 PM today</span>
                      </div>
                      <!-- /.user-block -->
                      <h3>{{$pesan->judul}}</h3>
                      <p>
                        {{$pesan->isi}}
                      </p>

                      <p>
                      @if($pesan->is_liked_by_auth_user())
                        <a href="{{route('pesan.unlike', ['id=>$pesan->id])}}"><i class="far fa-thumbs-up"></i>Unlike<span class="badge">{{$pesan->likes->count()}}</span></a>
                      @else
                        <a href="{{route('pesan.like', ['id=>$pesan->id])}}"><i class="far fa-thumbs-up"></i>Like <span class="badge">{{$pesan->likes->count()}}</span></a>
                        
                      </p>
                    </div>
                    <!-- /.post -->
                  </div>
                </div>
                <!-- /.tab-content -->
              </div>
@endsection