@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pesan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                @endif
                <a class="btn btn-info" href="/pertanyaan/create"> Buat Pesan</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">ID</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pesan as $key=>$post)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $post->judul }} </td>
                        <td> {{ $post->isi }} </td>
                        <td style="display: flex;">
                            <a href="/pesan/{{$post->id}}" class="btn btn-info btn-sm">Lihat</a>
                            <a href="/pesan/{{$post->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            <form action="/pesan/{{$post->id}}" method="post">
                                @csrf
                                @method("DELETE")
                                <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                        <tr> 
                            <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
</div>
@endsection