<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view('welcome');
});

Route::get('/pesan', 'PesanController@index');
Route::get('/pesan/create', 'PesanController@create');
Route::post('/pesan', 'PesanController@store');
Route::get('/pesan/{pesan_id}', 'PesanController@show');
Auth::routes();
Route::get('/user/{user_id}', 'UserController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pesan/{pesan_id}', 'PesanController@like');
Route::get('/pesan/{pesan_id}', 'PesanController@unlike');
