<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable=['pesan_id', 'user_id'];
    public function pesan(){
        return $this->belongsTo('App\Pesan');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
