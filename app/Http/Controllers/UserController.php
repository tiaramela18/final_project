<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function index(){
        // $pesan = DB::table('pesan')->get();
        // return view('posts.index', compact('pesan'));
        $tampilkan_data = Auth::user()->pesan()->paginate(10);
        //$jumlah_data = count($tampilkan_data['data']);
        return view('posts.index', compact('tampilkan_data'));
    }
   
}
