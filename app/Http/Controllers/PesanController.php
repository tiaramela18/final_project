<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use App\Pesan;
use App\User;
use App\Like;
use App\Carbon;
use Auth;
use DB;

class PesanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function create(){
        return view('posts.create2');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            "judul"=>'required', 
            "isi"=>'required',
            "tanggal"=>'required',
            "control"=>'required',
            "email"=>'required',
        ]);
        $file = $request->file('image');
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);
        $query = DB::table('pesan')->insert([
            "judul"=>$request["judul"], 
            "isi"=>$request['isi'],
            "image"=>$request['image'],
            "tanggal"=>$request['tanggal'],
            "control"=>$request['control'],
            "email"=>$request['email'],
        ]);
        Alert::success('Berhasil', 'Pesan berhasil dibuat!');
        return redirect('/pesan');
        //->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function index(){
        $pesan = DB::table('pesan')->get();
        return view('posts.index2', compact('pesan'));
    }

    public function show($pesan_id){
        $pesan = DB::table('pesan')->where('id', $pesan_id)->first();
        return view('posts.show', compact('pesan'));
    }

    // public function like($pesan_id){
    //     Like::create([
    //         'pesan_id'=>$pesan_id,
    //         'user_id'=>Auth::id()
    //     ]);
    // }

    public function unlike($pesan_id){
         $like =  Like::where('id', $pesan_id)->where('user_id', Auth::id())->first();
         $like->delete();
     }
}
