<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menulis extends Model
{
    public function pesan(){
    return $this->belongsTo('App\Pesan');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
